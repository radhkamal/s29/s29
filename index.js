const express = require("express");
const app = express();
const port = 4000;

// Setup for allowing server to handle data from requests(client)
// Allows app to read json data

app.use(express.json());

app.get('/', (request, response) => {
	response.send("Hello World from our new ExpressJS API!")
});

app.get('/hello', (request, response) => {
	response.send("Hello, Batch 157!")
});

/*app.post('/hello', (request, response) => {
	console.log(request.body);
	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`);
});*/

app.post('/hello', (request, response) => {
	console.log(request.body);
	response.send(`I am ${request.body.firstName} ${request.body.lastName}! I am ${request.body.age} years old, and I live at ${request.body.address}.`);
});

let users = [];

app.post('/signup', (request, response) => {
	console.log(request.body);

	if ((request.body.username != "") && (request.body.password != "")) {

		users.push(request.body);
		response.send(`User ${request.body.username} successfully registered.`)
	} else {
		response.send(`Please input BOTH username and password.`)
	}
});

app.put('/change-password', (request, response) => {
	console.log(request.body);

	let message;
	for (let i = 0; i < users.length; i++) {

		if (request.body.username === users[i].username) {

			users[i].password = request.body.password;
			message = `User ${request.body.username}'s password has been updated.`;
			break;
		} else {
			message	= `User does not exist`;
		}
	}

	response.send(message);
})

/*


Instruction:
1. Create a GET route that will access the "/home" route that will print out a simple message.
2. Process a GET request at the "/home" route using postman.
3. Create a GET route that will access the "/users" route that will retrieve all the users in the mock database.
4. Process a GET request at the "/users" route using postman.
5. Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.
6. Process a DELETE request at the "/delete-user" route using postman.
7. Export the Postman collection and save it inside the root folder of our application.
8. Create a git repository named S29.
9. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
10. Add the link in Boodle.

*/


// 1. Create a GET route that will access the "/home" route that will print out a simple message.
// 2. Process a GET request at the "/home" route using postman.

app.get('/home', (request, response) => {

	console.log(request.body);

	response.send(`Welcome to the home page!`);

});


// 3. Create a GET route that will access the "/users" route that will retrieve all the users in the mock database.

// 4. Process a GET request at the "/users" route using postman.

app.get('/users', (request, response) => {

	console.log(request.body);

	response.send(users);

});

// 5. Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.
// 6. Process a DELETE request at the "/delete-user" route using postman.

app.delete('/delete-user', (request, response) => {

	console.log(request.body);

	let user = request.body;

	for (let i = 0; i < users.length; i++) {

		if (user.username === users[i].username) {

			users.splice(i, 1);

		} 
	}

	response.send(`Username ${user.username} has been deleted.`);
});


app.listen(port, () => {console.log(`Server is running at port ${port}`)});